package oop;

import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;

public class Game {
    Deck pakk = new Deck();
    Table laud = new Table();
    ArrayList<Player> mangijad = new ArrayList<>();
    Pane canvas= new VBox();

    public Game(int mangijate_arv){
        for (int i=0;i<mangijate_arv;i++){
            this.mangijad.add(new Player());
        }
    }


    public Pane getGame() {

        int mangus_kaarte = 0;
        for (Player elem : mangijad) {
            mangus_kaarte += elem.getKaesKaarte();
        }

        Pane laual_kaardid = new HBox();
        Pane current_player_cards = new HBox();
        Pane other_player_cards = new HBox();

        for (Player current_mangija : mangijad) {

            //on table cards
            for (Pile pile : laud.getHunnikud()) {
                for (Card card : pile.ootab_kaarte) {
                    laual_kaardid.getChildren().addAll(card.getKaart_img());
                }
            }

            //current turn player cards
            for (Card kaart : current_mangija.getKasi()) {
                current_player_cards.getChildren().addAll(kaart.getKaart_img());
            }

            //other player cards
            for (Player teine_mangija : mangijad) {
                if (teine_mangija != current_mangija) {
                    for (Card kaart : teine_mangija.getKasi()) {
                        other_player_cards.getChildren().addAll(kaart.getKaart_tagune());
                    }
                }
            }

        }
        canvas.getChildren().addAll(laual_kaardid, current_player_cards, other_player_cards);
        return canvas;
    }

}
