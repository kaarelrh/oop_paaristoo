package oop;

import java.util.concurrent.atomic.AtomicInteger;

public class StartGameException extends Exception {
    public StartGameException(AtomicInteger message) {
        super(message.toString());
    }
}
