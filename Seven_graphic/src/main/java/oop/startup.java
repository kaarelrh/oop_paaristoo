package oop;

import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Integer.parseInt;

public class startup {
    ArrayList<Button> Anupud=new ArrayList<>();

    public Scene getAlgus() throws StartGameException {
        Pane algus= new Pane();
        Scene startup = new Scene(algus,1280, 800, Color.WHITESMOKE);
        Text tekst1= new Text();
        int mangijate_arv_int=1;

        tekst1.setText("Mängu eesmörk on saada enda kaartidest lahti.\n" +
                "Mängu alguses jagatakse teile 5 kaarti. Nende lauale käimiseks peab lauas olema sellele eelnev kaart. \n" +
                "Näiteks kui mängulaual on olemas risti 7, siis saab tema kõrvale käia risti 6 või risti 8 jne.\n" +
                "Kaardi käimiseks vajuta selle peale.\n" +
                "Kui käia ei saa tuleb üks kaart juurde võtta, selleks vajutada kaardipakile.\n" +
                "Mängu saab alustada ainult 7-ga. ");
        tekst1.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 24));
        tekst1.setX(40);
        tekst1.setY(180);

        Text tekst2= new Text();
        tekst2.setText("Seven");
        tekst2.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 80));
        tekst2.setX(460);
        tekst2.setY(100);

        Text tekst3= new Text();
        tekst3.setText("Vali mängijate arv");
        tekst3.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 40));
        tekst3.setX(420);
        tekst3.setY(420);

        Pane nupud= new Pane();

        for( int i=0; i < 5; i++){
            Button btn=new Button();
            Anupud.add(btn);
            if (i==0){
                btn.setText(i+1+" Mängija");
            }else{
                btn.setText(i+1+" Mängijat");
            }

            btn.setLayoutX(60+i*240);
            btn.setLayoutY(480);
            btn.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 30));
            btn.setOnAction(event -> {
                Game current_game=new Game(valitudnupp(btn));
                Scene game= new Scene(current_game.getGame(),1280,800, Color.DARKGREEN);
                //startup=game;
            });
            nupud.getChildren().addAll(btn);
        }

        Button btn= new Button();
        btn.setText("Uus mäng");
        btn.setLayoutX(40);
        btn.setMinWidth(550);
        btn.setLayoutY(600);
        btn.setMinHeight(180);
        btn.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 30));
        btn.setOnAction((ActionEvent event) -> {});
        Button btn1= new Button();
        btn1.setText("Jätka eelmist //not implemented");
        btn1.setLayoutX(690);
        btn1.setMinWidth(550);
        btn1.setLayoutY(600);
        btn1.setMinHeight(180);
        btn1.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 30));
        //btn1.setOnAction(event -> );

        algus.getChildren().addAll(tekst1,tekst2,tekst3,nupud,btn,btn1);
        return startup;
    }

    private int valitudnupp(Button btn){
        for (Button nupp:Anupud){
            nupp.setTextFill(Color.BLACK);
        }
        btn.setTextFill(Color.GREEN);
        return parseInt(btn.getText().substring(0, 1));
    }
}
