import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

import java.awt.*;
import java.util.Arrays;
import java.util.List;

public class Card {
    private int masti_nr; //1 ruutu, 2 risti, 3 artu, 4 poti
    private int vaartus; //1 äss, 2-10 numbrid, 11 poiss, 12 emand, 13 kuningas
    private int designator; //individuaalne number
    static List<String> nimed= Arrays.asList("Äss","2","3","4","5","6","7","8","9","10","Poiss","Emand","Kuningas");

    public Card(int designator,int masti_nr, int vaartus) {
        this.designator = designator;
        this.masti_nr = masti_nr;
        this.vaartus = vaartus;
    }

    public String getMast() {
        if (masti_nr== 1){
            return "Ruutu";
        }
        else if (masti_nr== 2){
            return  "Risti";
        }
        else if (masti_nr== 3){
            return  "Ärtu";
        }
        else return "Poti";
    }

    public int getMasti_nr(){
        return masti_nr;
    }

    public int getVaartus(){
        return vaartus;
    }

    public String getNimi() {
        return (nimed.get(vaartus - 1));
    }

    //igal kaardil oma jrk number pakis
    public int getDesignator(){
        return designator;
    }

    public String getKaart(){
        return this.getMast() +" " + this.getNimi();
    }

    //tagastab mis designatoriga kaart tema peale laheb, va 7, mis tagastab enda designatori sest sinna peale laheb 2 kaarti mis vajab lisa loogikat//klassis pile
    public int getjargmine(){
        if (vaartus== 7){
            return getDesignator();
        }
        else if (vaartus<= 6){
            return getDesignator()-1;
        }
        else {
            return getDesignator()+1;
        }
    }
    public int getEelmine(){
        if (vaartus== 7){
            return getDesignator();
        }
        else if (vaartus<= 6){
            return getDesignator()+1;
        }
        else {
            return getDesignator()-1;
        }
    }

    public Pane getKaart_img(){
        Pane pilt=new Pane();
        Rectangle taust=new Rectangle(100,200, Color.WHITE);

        return(pilt);
    }

}
