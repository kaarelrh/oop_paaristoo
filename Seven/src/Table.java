import java.util.ArrayList;

public class Table {
    ArrayList<Pile> hunnikud= new ArrayList<>();

    public Table() {
        for (int i = 1; i < 5; i++) {
            hunnikud.add(new Pile(i));
        }
    }

    public void mangi_kaart(Card y){
        for (int i=0;i<hunnikud.size();i++){
            if (hunnikud.get(i).getMast() == y.getMasti_nr()){
                hunnikud.get(i).lisa_kaart(y);
            }
        }
    }

    public ArrayList<Pile> getHunnikud() {
        return hunnikud;
    }
}
