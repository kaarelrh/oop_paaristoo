import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Player {
    private ArrayList<Card> kasi;
    private ArrayList<Integer> kaardi_numbrid;

    public Player() {
        this.kasi = new ArrayList<>();
    }

    public ArrayList<Card> getKasi() {
        return kasi;
    }

    public void first_draw(ArrayList<Card> a){
        kasi.addAll(a);
    }

    public void draw(Card b){

        kasi.add(b);
        System.out.println("Said kaardi (" + b.getDesignator() + ") " + b.getKaart());
    }

    public ArrayList<Integer> getKaardiNumbrid(){
        kaardi_numbrid = new ArrayList<>();
        for (int i= 0; i<kasi.size();i++){
            kaardi_numbrid.add(kasi.get(i).getDesignator());
        }
        Collections.sort(kaardi_numbrid);
        return kaardi_numbrid;
    }

    public void play_card(Card c){
        kasi.remove(c);
    }

}
