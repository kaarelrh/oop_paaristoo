import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Deck pakk = new Deck();
        Table laud = new Table();
        ArrayList<Player> mangijad = new ArrayList<>();

        Player mangija1 = new Player();
        mangijad.add(mangija1);

        System.out.println("Mängu eesmörk on saada enda kaartidest lahti.\n" +
                "Mängu alguses jagatakse teile 5 kaarti. Nende mängu lauale käimiseks peab olema olemas eelnev kaart. \n" +
                "Näiteks kui mängulaual on olemas risti 7, siis saab tema kõrvale käia risti 6 või risti 8 jne.\n" +
                "Kui käia ei saa tuleb üks kaart juurde võtta. Ja kui kaardipakk on otsa saanud, siis saad nii palju miinuspunkte kui kaarte on alles.\n" +
                "Mängu saab alustada ainult 7-ga. Iga käsu käivitamiseks tuleb vajutada ENTER(ENTER-t tuleb vajutada nii peale käin kösklust kui ka arvu sisestamist)");

        Scanner scan = new Scanner(System.in);
        System.out.println("Sisesta enda mängija nimi ja vajuta ENTER: ");
        String nimi = scan.next();

        if (pakk.getAllesKaarte() > 5) {
            mangija1.first_draw(pakk.draw5Card());
        }


        //&& mangija1.getKasi().size() != 0)
        while (pakk.getAllesKaarte() > 0 && mangija1.getKasi().size() != 0) {

            for (int j = 0; j < mangijad.size(); j++) {
                //System.out.println("Kaarte kaardipakis: " + pakk.getAllesKaarte());


                //for (int i=0; i<pakk.getAllesKaarte();i++){ System.out.println(pakk.getPakk().get(i).getDesignator()); }
                System.out.println("Hetkel mängulaual olevad kaardid:");
                System.out.println("----------------------------------");
                //teed siia meetodi jne et prindiks välja, samas formaadis nagu mängija kaardid, et mis hetkel lauas on, (for i in deck-> for i in hunnik-> for i in ootabkaarte print()
                for (Pile pile:laud.getHunnikud()
                     ) {
                    for (Card card: pile.ootab_kaarte
                         ) {
                        System.out.println("("+card.getDesignator()+") "+card.getKaart());
                        mangija1.play_card(card);

                    }
                }
                System.out.println("----------------------------------");



                Player current_mangija = mangijad.get(j);

                System.out.println("\n"+nimi + " siin on teie kaardid:");
                ArrayList<String> kaardidkäes = new ArrayList<>();
                for (int i = 0; i < current_mangija.getKasi().size(); i++) {
                    String listielement = "(" + current_mangija.getKasi().get(i).getDesignator() + ") " + current_mangija.getKasi().get(i).getKaart();
                    kaardidkäes.add(listielement);

                }
                Collections.sort(kaardidkäes);
                for (String e:kaardidkäes) {
                    System.out.println(e);
                }

                System.out.println("Mis on teie soovitud käik?(võtmiseks tuleb kirjutada võtan, käimiseks kirjutada käin ja seejärel kaardi ees olev sulgudes number-arvuti asetab ise õigesse kohta kaardi)");
                String sisend = scan.next().strip().toLowerCase();

                if (sisend.equals("võtan")) {
                    current_mangija.draw(pakk.drawCard());
                } else if (sisend.equals("käin")) {
                    System.out.println("Sisesta kaardi nr: ");
                    int kaardinr = scan.nextInt();

                    if (current_mangija.getKaardiNumbrid().contains(kaardinr)){
                        for (int i = 0; i < current_mangija.getKasi().size(); i++) {
                            if (current_mangija.getKasi().get(i).getDesignator() == kaardinr) {
                                Card kaart1 = current_mangija.getKasi().get(i);
                                laud.mangi_kaart(kaart1);
                                }
                            }
                        }
                        else System.out.println("Teil ei ole sellist kaarti käes");



                }
                for (int i=0;i<4;i++){
                    System.out.println(" ");
                }
            }

            if (mangija1.getKasi().size() == 0)System.out.println("Oled võitnud");
            if (pakk.getAllesKaarte() == 0) {
                System.out.println("Kaardid otsas");
                System.out.println("Said " + mangija1.getKasi().size()+ " miinuspunkti.");
            }
        }
    }
}

