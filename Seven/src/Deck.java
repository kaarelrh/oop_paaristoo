import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class Deck {
    static int mastide_arv = 4;
    int mast = 1;
    static int kaartide_arv = 13;
    int kaart = 1;
    private ArrayList<Card> pakk = new ArrayList<>();

    //loob paki kus 4*13=52 kaarti
    public Deck() {
        for (int i = 1; i <= 52; i++) {
            pakk.add(new Card(i, mast, kaart));
            if (i == 13 || i == 26 || i == 39) {
                mast++;
            }
            kaart++;
            if (kaart == 14) {
                kaart = 1;
            }
        }
    }

    public ArrayList<Card> getPakk() {
        return pakk;
    }

    //tagastab pakist suvalise kaardi ja eemaldab selle pakist
    public Card drawCard() {
        if (pakk.size() > 1) {
            int rand = ThreadLocalRandom.current().nextInt(0, this.pakk.size() - 1);
            Card valitud = this.pakk.get(rand);
            this.pakk.remove(valitud);
            return valitud;
        } else {
            Card valitud = pakk.get(0);
            this.pakk.remove(valitud);
            return valitud;
        }
    }

    public ArrayList<Card> draw5Card(){
        ArrayList<Card> valitud= new ArrayList<>();
        for (int i=0; i<5; i++){
            valitud.add(this.drawCard());
        }
        return valitud;
    }
    public int getAllesKaarte(){
        return this.pakk.size();
    }

}
